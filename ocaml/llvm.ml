(* TODO : extend when you extend the language *)

(* This file contains a simple LLVM IR representation *)
(* and methods to generate its string representation  *)

type llvm_type =
  | LLVM_type_i32
  | LLVM_type_i32_array of int
  | LLVM_type_void

type llvm_var = string
    
type llvm_value =
  | LLVM_i32 of int
  | LLVM_var of llvm_var
  | LLVM_void

type llvm_opt = 
  | LLVM_ICMP_NEQ
  | LLVM_ICMP_EQ
  | LLVM_ICMP_UGT
  | LLVM_ICMP_UGE
  | LLVM_ICMP_ULT
  | LLVM_ICMP_ULE
			 
type llvm_ir = (* type of generated IR *)
  { header: llvm_instr_seq; (* instructions to be placed before all code (global definitions) *)
    body: llvm_instr_seq;
  }
    
 and llvm_instr_seq = (* type of sequences of instructions *)
   | Empty
   | Atom of llvm_instr
   | Concat of llvm_instr_seq * llvm_instr_seq

 and llvm_instr = string (* type of instructions *)

(* empty IR *)
let empty_ir = {
  header = Empty;
  body = Empty;
}
		 
(* appending an instruction in the header: ir @^ i *)
let (@^) ir i = {
    header = Concat (ir.header, Atom i);
    body = ir.body;
  }
		 
(* appending an instruction in the body: ir @: i *)
let (@:) ir i = {
    header = ir.header;
    body = Concat (ir.body, Atom i);
  }
		 
(* concatenation of two IRs: ir1 @@ ir2 *)
let (@@) ir1 ir2 = {
    header = Concat (ir1.header, ir2.header);
    body = Concat (ir1.body, ir2.body);
}
		 
(* actual IR generation *)
let rec string_of_type = function
  | LLVM_type_i32 -> "i32"
  | LLVM_type_i32_array (size) -> "[" ^ string_of_int size ^ " x i32]"
  | LLVM_type_void -> "void"

and string_of_var x = x

and string_of_value = function
  | LLVM_i32 n -> string_of_int n
  | LLVM_var x -> string_of_var x
  | LLVM_void -> "void"

and string_of_opt = function
  | LLVM_ICMP_NEQ -> "ne"
  | LLVM_ICMP_EQ  -> "eq"
  | LLVM_ICMP_UGT -> "ugt"
  | LLVM_ICMP_UGE -> "uge"
  | LLVM_ICMP_ULT -> "ult"
  | LLVM_ICMP_ULE -> "ule"
		     
and string_of_ir ir =
  (* this header describe to LLVM the target
   * and declare the external function printf
   *)
  "; Target\n"
  ^ "target triple = \"x86_64-unknown-linux-gnu\"\n"
  ^ "; External declaration of the printf function\n"
  ^ "declare i32 @printf(i8* noalias nocapture, ...)\n"
  ^ "; External declaration of the scanf function\n"
  ^ "declare i32 @scanf (i8* noalias nocapture, ...)\n"
  ^ "\n; Actual code begins\n"
  ^ string_of_instr_seq ir.header
  ^ "\n\n"
  ^ string_of_instr_seq ir.body

and string_of_instr_seq = function
  | Empty -> ""
  | Atom i -> i
  | Concat (li1,li2) -> string_of_instr_seq li1 ^ string_of_instr_seq li2

and string_of_instr i = i

(* functions for the creation of various instructions *)
let llvm_add ~(res_var : llvm_var) ~(res_type : llvm_type) ~(left : llvm_value) ~(right : llvm_value) : llvm_instr =
  string_of_var res_var ^ " = add " ^ string_of_type res_type ^ " " ^ string_of_value left ^ ", " ^ string_of_value right ^ "\n"

let llvm_sub ~(res_var : llvm_var) ~(res_type : llvm_type) ~(left : llvm_value) ~(right : llvm_value) : llvm_instr =
  string_of_var res_var ^ " = sub nuw nsw " ^ string_of_type res_type ^ " " ^ string_of_value left ^ ", " ^ string_of_value right ^ "\n"

let llvm_mul ~(res_var : llvm_var) ~(res_type : llvm_type) ~(left : llvm_value) ~(right : llvm_value) : llvm_instr =
  string_of_var res_var ^ " = mul " ^ string_of_type res_type ^ " " ^ string_of_value left ^ ", " ^ string_of_value right ^ "\n"

let llvm_div ~(res_var : llvm_var) ~(res_type : llvm_type) ~(left : llvm_value) ~(right : llvm_value) : llvm_instr =
  string_of_var res_var ^ " = sdiv " ^ string_of_type res_type ^ " " ^ string_of_value left ^ ", " ^ string_of_value right ^ "\n"

let llvm_store ~(target_var : llvm_value) ~(res_type : llvm_type) ~(assign_value : llvm_value) : llvm_instr =
  "store " ^ string_of_type res_type ^ " " ^ string_of_value assign_value ^ ", " ^ string_of_type res_type ^ "* " ^ string_of_value target_var ^ "\n"

let llvm_load ~(res_var : llvm_var) ~(res_type : llvm_type) ~(from_addr : llvm_value) : llvm_instr =
  string_of_var res_var ^ " = load " ^  string_of_type res_type ^ ", " ^ string_of_type res_type ^ "* " ^ string_of_value from_addr ^ "\n"

let llvm_return ~(ret_type : llvm_type) ~(ret_value : llvm_value) : llvm_instr =
  match ret_type with
  | LLVM_type_void -> "ret " ^ string_of_type ret_type ^ "\n"
  | _ -> "ret " ^ string_of_type ret_type ^ " " ^ string_of_value ret_value ^ "\n"

let llvm_alloca ~(res_var : llvm_var) ~(res_type : llvm_type) : llvm_instr =
  string_of_var res_var ^ " = alloca " ^ string_of_type res_type ^ "\n"

let llvm_getelementptr ~(res_var : llvm_var) ~(res_type : llvm_type) ~(array_value : llvm_var) ~(array_element : llvm_value) : llvm_instr = 
  string_of_var res_var ^ " = getelementptr " ^ string_of_type res_type ^ ", "
    ^ string_of_type res_type ^ "* " ^ string_of_var array_value ^ ", i64 0, i32 "
    ^ string_of_value array_element ^ "\n"

let llvm_br ~(br_dest : llvm_var) : llvm_instr =
  "br label %" ^ string_of_var br_dest ^ "\n"

let llvm_br_cond ~(temp_var : llvm_var) ~(icmp_opt : llvm_opt) ~(res_val : llvm_value) ~(br_true : llvm_var) ~(br_false : llvm_var) : llvm_instr =
  string_of_var temp_var ^ " = icmp " ^ string_of_opt icmp_opt ^ " i32 " ^ string_of_value res_val ^ ", 0\n" ^ 
  "br i1 " ^ string_of_var temp_var ^ ", label %" ^ string_of_var br_true ^ ", label %" ^ string_of_var br_false ^ "\n"

let llvm_label ~(label : llvm_var) : llvm_instr =
  "\n" ^ string_of_var label ^ ":\n"

let llvm_read ~(global_var : llvm_var) ~(vars : llvm_value list) ~(global_var_size : int) : llvm_instr =
  let rec str_of_vars = function l ->
    match l with
    | [] -> ""
    | head :: tail -> ", i32* " ^ string_of_value head ^ str_of_vars tail
  in
  "call i32 (i8*, ...) @scanf (i8* getelementptr inbounds (["
  ^ (string_of_int global_var_size) ^ " x i8], [" ^ (string_of_int global_var_size) ^ " x i8]* " 
  ^ (string_of_var global_var) ^ ", i64 0, i64 0)" ^ (str_of_vars vars) ^ ")\n"

let llvm_print ~(global_var : llvm_var) ~(vars : llvm_value list) ~(global_var_size : int) : llvm_instr =
  let rec str_of_vars = function l ->
    match l with
    | [] -> ""
    | head :: tail -> ", i32 " ^ string_of_value head ^ str_of_vars tail
  in
  "call i32 (i8*, ...) @printf (i8* getelementptr inbounds (["
  ^ (string_of_int global_var_size) ^ " x i8], [" ^ (string_of_int global_var_size) ^ " x i8]* " 
  ^ (string_of_var global_var) ^ ", i64 0, i64 0)" ^ (str_of_vars vars) ^ ")\n"

let llvm_call ~(var : llvm_var) ~(funct : llvm_var) ~(fun_type : llvm_type) ~(vars : llvm_value list) : llvm_instr =
  let rec str_of_vars = function l ->
    match l with
    | [] -> ""
    | head :: [] -> "i32 " ^ string_of_value head
    | head :: tail -> "i32 " ^ string_of_value head ^ ", " ^ str_of_vars tail
  in
  match fun_type with
    | LLVM_type_i32 -> string_of_var var ^ " = call " ^ string_of_type fun_type ^ " @" ^ string_of_var funct ^ "(" ^ str_of_vars vars ^ ")\n"
    | LLVM_type_void -> "call " ^ string_of_type fun_type ^ " @" ^ string_of_var funct ^ "(" ^ str_of_vars vars  ^ ")\n"
    | _ -> failwith "Incorrect function return type"

let llvm_global ~(global_var : llvm_var) ~(str : string) ~(global_var_size : int) : llvm_instr =
  string_of_var global_var ^ " = global [" ^ string_of_int global_var_size ^ " x i8] c\"" ^ str ^ "\"\n"

let llvm_define ~(ret_type : llvm_type) ~(funct_name : llvm_var) ~(funct_args : llvm_var list) : llvm_instr =
  let rec aux (l: llvm_var list) : string = 
    match l with
    | [] -> ""
    | [v] -> string_of_type LLVM_type_i32 ^ " %" ^ string_of_var v
    | v::tail -> string_of_type LLVM_type_i32 ^ " %" ^ string_of_var v ^ ", " ^ aux tail 
  in
  "define " ^ string_of_type ret_type ^ " @" ^ string_of_var funct_name ^ "(" ^ aux funct_args ^ ")\n"
									 
let llvm_prog (ir : llvm_ir) : llvm_ir =
  { header = ir.header;
    body = Atom (string_of_instr_seq ir.body);
  }
								
