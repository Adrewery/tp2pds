open ASD
open Llvm
open Utils
open SymbolTable

       
(* main function. returns only a string: the generated code *)
let rec ir_of_ast (prog : program) : llvm_ir = 
  let ir, t = ir_of_program prog in
  let ir = llvm_prog ir in
  ir

(* translation from VSL+ types to LLVM types *)
and llvm_type_of_asd_typ : typ -> llvm_type = function
  | Type_Int -> LLVM_type_i32
  | Type_Int_Array s -> LLVM_type_i32_array s
  | Type_Void -> LLVM_type_void

and ir_of_list_instr : instruction list -> symbol_table -> llvm_ir * symbol_table =
  function l -> function t ->
    (match l with
    | [] -> empty_ir, t
    | head :: tail ->
      let ir1, t1 = ir_of_instruction head t in
      let ir2, t2 = ir_of_list_instr tail t1 in
      let ir = ir1 @@ ir2 in
      ir, t2)

and ir_of_list_decl : declaration list -> symbol_table -> llvm_ir * symbol_table =
  function l -> function t ->
    (match l with
    | [] -> empty_ir, t
    | head :: tail ->
      let ir1, t1 = ir_of_declaration head t in
      let ir2, t2 = ir_of_list_decl tail t1 in
      let ir = ir1 @@ ir2 in
      ir, t2)

(* all expressions have type LLVM_type_i32 *)
(* they return code (llvm_ir) and expression result (llvm_value) *)
and ir_of_program : program -> llvm_ir * symbol_table =
  function prog ->
    match prog with
      | Program d -> 
        let ir, t = (ir_of_list_decl d []) in
        (all_proto_are_defined t); (* Verified that all prototypes are defined.*)
        ir, t

and ir_of_instruction : instruction -> symbol_table -> llvm_ir * symbol_table = 
  function instr -> function t ->
    match instr with
    | BlockInstruction (d, i) ->
      let ir1, t1 = ir_of_list_decl d t in
      let ir2, t2 = ir_of_list_instr i t1 in
      let ir = ir1 @@ ir2 in
      ir, t
    | AssignInstruction (name, e) ->
       let ir1, v1, t1 = ir_of_variable name t in
       let ir2, v2 = ir_of_expression e t1 in
       (* Next function is used to verify that a variable is declared. *)
       let _, _ = (match name with 
        | Variable (id, size) -> (
          match lookup t id with 
          | Some (VariableSymbol (id_t, _, llvm_id)) -> id_t, llvm_id
          | _ -> failwith ("Undeclared variable: " ^ id) ))
       in
       let ir = ir1 @@ ir2 @: llvm_store ~target_var:v1 ~res_type:(LLVM_type_i32) ~assign_value:v2 in
       ir, t1
    | WhileInstruction (cond, e) ->
      let ir1, v1 = ir_of_expression cond t in
      let x = newtmp() in
      let ir2, t2 = ir_of_instruction e t in
      let new_while_label = newlab "while" in
      let new_do_label = newlab "do" in
      let new_done_label = newlab "done" in
      let ir = ((empty_ir @: llvm_br ~br_dest:new_while_label)
        @: llvm_label ~label:new_while_label) @@ ((ir1 @: llvm_br_cond ~icmp_opt:LLVM_ICMP_NEQ ~temp_var:x ~res_val:v1 ~br_true: new_do_label ~br_false: new_done_label) @: llvm_label ~label:new_do_label)
        @@ ((ir2 @: llvm_br ~br_dest:new_while_label) @: llvm_label ~label:new_done_label)
      in
      ir, t
    | DoWhileInstruction (e, cond) ->
      let ir0, t0 = ir_of_instruction e t in
      let ir1, v1 = ir_of_expression cond t0 in
      let x = newtmp() in
      let new_while_label = newlab "dowhile" in
      let new_done_label = newlab "done" in
      let ir = (((empty_ir @: llvm_br ~br_dest:new_while_label)
        @: llvm_label ~label:new_while_label) @@ (ir0 @@ (ir1 @: llvm_br_cond ~icmp_opt:LLVM_ICMP_NEQ ~temp_var:x ~res_val:v1 ~br_true:new_while_label ~br_false:new_done_label)
        @: llvm_br ~br_dest:new_while_label) @: llvm_label ~label:new_done_label)
      in
      ir, t
    | ForInstruction (name, start, stop, e) ->
      let ir0, t0 = ir_of_declaration (VariableDeclaration name) t in
      let ir1, t1 = ir_of_instruction (AssignInstruction(name, start)) t0 in   
      let new_for_label = newlab "for" in
      let ir2, v2 = ir_of_expression (SubExpression(stop, LoadExpression(name))) t1 in
      let x = newtmp() in
      let new_do_label = newlab "do" in
      let ir3, t3 = ir_of_instruction (BlockInstruction ([], [e; AssignInstruction(name, AddExpression(LoadExpression(name), IntegerExpression 1))])) t1 in
      let new_done_label = newlab "done" in
      let ir = ir0 @@ ((ir1 @: llvm_br ~br_dest:new_for_label) @: llvm_label ~label:new_for_label)
        @@ ((ir2 @: (llvm_br_cond ~icmp_opt:LLVM_ICMP_NEQ ~temp_var:x ~res_val:v2 ~br_true: new_do_label ~br_false: new_done_label)) @: llvm_label ~label:new_do_label)
        @@ ((ir3 @: llvm_br ~br_dest:new_for_label) @: llvm_label ~label:new_done_label)
      in
      ir, t
    | IfInstruction(eif, ethen, eelse) ->
      let ir1, v1 = ir_of_expression eif t in
      let new_then_label = newlab "then" in
      let ir2, t2 = ir_of_instruction ethen t in
      let x = newtmp () in
      (match eelse with
      | Some e ->
        let new_else_label = newlab "else" in
        let ir3, t3 = ir_of_instruction e t in
        let new_endfi_label = newlab "fi" in
        let ir = 
          ((ir1 @: llvm_br_cond ~icmp_opt:LLVM_ICMP_NEQ ~temp_var:x ~res_val:v1 ~br_true:new_then_label ~br_false:new_else_label) @: (llvm_label ~label:new_then_label)) 
          @@ ((ir2 @: llvm_br ~br_dest:new_endfi_label) @: llvm_label ~label:new_else_label) 
          @@ ((ir3 @: llvm_br ~br_dest:new_endfi_label) @: llvm_label ~label:new_endfi_label)
        in
        ir, t
      | None -> 
        let new_endfi_label = newlab "fi" in
        let ir = 
          ((ir1 @: llvm_br_cond ~icmp_opt:LLVM_ICMP_NEQ ~temp_var:x ~res_val:v1 ~br_true:new_then_label ~br_false:new_endfi_label) @: (llvm_label ~label:new_then_label)) 
          @@ ((ir2 @: llvm_br ~br_dest:new_endfi_label) @: llvm_label ~label:new_endfi_label)
        in
        ir, t
      )
    | ReturnInstruction e ->
      let ir1, v1 = ir_of_expression e t in
      let ir = ir1 @: llvm_return ~ret_type:LLVM_type_i32 ~ret_value:v1
      in
      ir, t
    | CallInstruction e ->
      let ir1, v1 = ir_of_expression e t in
      ir1, t
    | PrintInstruction l ->
      let rec compute_variable = function l ->
        match l with
        | [] ->  ""
        | head::tail -> 
          match head with
          | StringExpression e -> e ^ compute_variable tail
          | _ -> "%d" ^ compute_variable tail
      in
      let rec compute_expr = function l ->
        match l with
        | [] -> empty_ir, []
        | head::tail ->
          match head with
          | StringExpression _ -> compute_expr tail
          | _ -> 
            let ir_t, v_t = ir_of_expression head t in
            let ir_next, vars_next = compute_expr tail in
            ir_t @@ ir_next, (v_t :: vars_next)
      in
      let x = newglob ".fmt" in
      let glob_str, glob_str_size = string_transform (compute_variable l) in
      let ir1, list_v = compute_expr l in
      let ir = ((ir1 @^ llvm_global ~global_var:x ~str:glob_str ~global_var_size:glob_str_size) @: (llvm_print ~global_var:x ~vars:list_v ~global_var_size:glob_str_size)) in
      ir, t
    | ReadInstruction v ->
      let x = newglob ".fmt" in
      let rec extract_infos = function l ->
        match l with
        | [] -> empty_ir, [], ""
        | head::tail -> 
          let _ = 
            match head with
            | Variable (id, size) -> id
          in
          let ir1, v1, t1 = ir_of_variable head t in
          let ir_t, id_t, str_t = extract_infos tail in
          ir1 @@ ir_t, v1 :: id_t, "%d" ^ str_t
      in
      let ir1, vars, glob_str = extract_infos v in
      let glob_str, glob_str_size = string_transform glob_str in
      let ir = ((ir1 @^ llvm_global ~global_var:x ~str:glob_str ~global_var_size:glob_str_size) @: (llvm_read ~global_var:x ~vars:vars ~global_var_size:glob_str_size)) in
      ir, t

and ir_of_expression : expression -> symbol_table -> llvm_ir * llvm_value =
  function e -> function t -> 
    match e with
    | IntegerExpression i ->
       empty_ir, LLVM_i32 i
    | StringExpression s ->
       empty_ir, LLVM_void
    | CallExpression (funct, args, return_type) ->
      (is_function_defined t funct);
      (is_function_call_correct t funct (List.length args));
      (is_function_correct_return_type t funct return_type);
      let rec compute_expr = function l ->
        match l with
        | [] -> empty_ir, []
        | head::tail ->
            let ir_t, v_t = ir_of_expression head t in
            let ir_next, vars_next = compute_expr tail in
            ir_t @@ ir_next, (v_t :: vars_next)
      in
      let ir1, v_list = compute_expr args in
      let funct_type = match (lookup t funct) with
        | Some (FunctionSymbol {return_type = funct_type; _}) -> funct_type
        | _ -> failwith "Not a valid function"
      in
      let x = newtmp () in
      let ir = ir1 @: llvm_call ~var:x ~funct:funct ~fun_type:(llvm_type_of_asd_typ funct_type) ~vars:v_list in
      ir, LLVM_var x
    | AddExpression (e1,e2) ->
       let ir1, v1 = ir_of_expression e1 t in
       let ir2, v2 = ir_of_expression e2 t in
       let x = newtmp () in
       let ir = ir1 @@ ir2 @: llvm_add ~res_var:x ~res_type:LLVM_type_i32 ~left:v1 ~right:v2 in
       ir, LLVM_var x
    | SubExpression (e1,e2) ->
       let ir1, v1 = ir_of_expression e1 t in
       let ir2, v2 = ir_of_expression e2 t in
       let x = newtmp () in
       let ir = ir1 @@ ir2 @: llvm_sub ~res_var:x ~res_type:LLVM_type_i32 ~left:v1 ~right:v2 in
       ir, LLVM_var x
    | MulExpression (e1,e2) ->
       let ir1, v1 = ir_of_expression e1 t in
       let ir2, v2 = ir_of_expression e2 t in
       let x = newtmp () in
       let ir = ir1 @@ ir2 @: llvm_mul ~res_var:x ~res_type:LLVM_type_i32 ~left:v1 ~right:v2 in
       ir, LLVM_var x
    | DivExpression (e1,e2) ->
       let ir1, v1 = ir_of_expression e1 t in
       let ir2, v2 = ir_of_expression e2 t in
       let x = newtmp () in
       let ir = ir1 @@ ir2 @: llvm_div ~res_var:x ~res_type:LLVM_type_i32 ~left:v1 ~right:v2 in
       ir, LLVM_var x
    | LoadExpression e ->
       let ir1, v1, t1 = ir_of_variable e t in
       let x = newtmp () in
       let ir = ir1 @: llvm_load ~res_var:x ~res_type:LLVM_type_i32 ~from_addr: v1 in
       ir, LLVM_var x
    | TernaireExpression (cond, expr_if, expr_else) ->
       let new_if_label = newlab "ternary" in
       let ir_var, t_var = ir_of_declaration (VariableDeclaration (Variable (new_if_label ^ "_var", None))) t in
       let ir1, v1 = ir_of_expression cond t_var in
       let comp_var = newtmp () in
       let ir2, t2 = ir_of_instruction (AssignInstruction (Variable (new_if_label ^ "_var", None), expr_if)) t_var in
       let new_then_label = newlab "ternary_true" in 
       let ir3, t3 = ir_of_instruction (AssignInstruction (Variable (new_if_label ^ "_var", None), expr_else)) t_var in
       let new_else_label = newlab "ternary_false" in
       let new_done_label = newlab "ternary_done" in
       let ir_load, v_load = ir_of_expression (LoadExpression (Variable (new_if_label ^ "_var", None))) t_var in

       let ir = 
        ((empty_ir @: llvm_br ~br_dest:new_if_label)
        @: llvm_label ~label:new_if_label)
        @@ ir_var
        @@ ((ir1
        @: llvm_br_cond ~icmp_opt:LLVM_ICMP_NEQ ~temp_var:comp_var ~res_val:v1 ~br_true:new_then_label ~br_false:new_else_label) 
        @: llvm_label ~label:new_then_label)
        @@ ((ir2 
        @: llvm_br ~br_dest:new_done_label)
        @: llvm_label ~label:new_else_label)
        @@ ((ir3
        @: llvm_br ~br_dest:new_done_label)
        @: llvm_label ~label:new_done_label)
        @@ ir_load
       in
       ir, v_load

and ir_of_declaration : declaration -> symbol_table -> llvm_ir * symbol_table =
  function e -> function t -> 
    match e with
    | Declarations e ->
      ir_of_list_decl e t
    | VariableDeclaration v ->
        (match v with
        | Variable (id,size) ->
         let ir, t = (match size with
           | Some (IntegerExpression i) -> 
           let llvm_id = newvar id in
           let t2 = add t (VariableSymbol ((Type_Int_Array i), id, llvm_id)) in
           empty_ir @: llvm_alloca ~res_var:(llvm_id) ~res_type:(LLVM_type_i32_array i), t2
           | _ -> 
           let llvm_id = newvar id in
           let t2 = add t (VariableSymbol (Type_Int, id, llvm_id)) in
           empty_ir @: llvm_alloca ~res_var:(llvm_id) ~res_type:LLVM_type_i32, t2)
         in 
         ir, t)
    | FunctionDeclaration (typ, name, args, instr) ->
      (is_function_already_declared t name typ (List.length args));
      let rec aux l = match l with
        | [] -> [], []
        | arg::tail -> 
          match arg with
          | Variable (n, _) -> n::(fst (aux tail)), VariableSymbol(Type_Int, n, n)::(snd (aux tail))
      in 
      let rec aux_init_var l t = match l with
        | [] -> empty_ir, t
        | arg::tail ->
          match arg with
          | Variable (n, _) -> 
            let ir_alloca, t_alloca = ir_of_declaration (VariableDeclaration arg) t in
            let id_type, llvm_id = 
              match (lookup t_alloca n) with
              | Some (VariableSymbol (id_type, _, llvm_id)) -> id_type, llvm_id
              | _ -> failwith "Undeclared variable"
            in
            let ir_next, t_next = aux_init_var tail t_alloca in 
            let ir_assign = ir_alloca @: llvm_store ~target_var:(LLVM_var llvm_id) ~res_type:LLVM_type_i32 ~assign_value:(LLVM_var ("%" ^ n)) in
            ir_assign @@ ir_next, t_next 
      in
      let id_list, new_symbol_table = aux args in
      let t0 = add t (FunctionSymbol({return_type = typ; identifier = name; arguments = new_symbol_table; state = Defined; })) in
      let ir_var, t_var = aux_init_var args t in
      let ir1, t2 = ir_of_instruction instr t_var in
      let ir = (empty_ir @: ((llvm_define ~ret_type:(llvm_type_of_asd_typ typ) ~funct_name:name ~funct_args:id_list) ^ "{\n")) @@ ((ir_var @@ (ir1 @: llvm_return ~ret_type:(llvm_type_of_asd_typ typ) ~ret_value:(LLVM_i32 0))) @: "}\n\n") in
      ir, t0
    | ProtoDeclaration (typ, name, args) ->
      let rec aux l = match l with
        | [] -> []
        | arg::tail -> 
          match arg with
          | Variable (n, _) -> VariableSymbol(Type_Int, n, n) :: (aux tail)
      in 
      let new_symbol_table = aux args in
      let t0 = add t (FunctionSymbol({return_type = typ; identifier = name; arguments = new_symbol_table; state = Declared; })) in
      empty_ir, t0

and ir_of_variable : variable -> symbol_table -> llvm_ir * llvm_value * symbol_table = 
  function v -> function t ->
    match v with
    | Variable (id,size) ->
      (match size with
        | Some s -> 
          let x = newtmp () in
          let id_type, llvm_id = match lookup t id with 
            | Some (VariableSymbol ((Type_Int_Array s), _, llvm_id)) -> (Type_Int_Array s), llvm_id
            | Some (VariableSymbol (_, _, _)) -> failwith ("Variable " ^ id ^ " is not an Array of Int")
            | _ -> failwith ("Undeclared variable: " ^ id)
          in
          let ir_s, v_s = ir_of_expression s t in
          let ir = ir_s 
            @: llvm_getelementptr ~res_var:x ~res_type:(llvm_type_of_asd_typ id_type) ~array_value:(llvm_id) ~array_element:v_s 
          in
          ir, LLVM_var x, t
        | None -> 
          let id_type, llvm_id = match lookup t id with 
            | Some (VariableSymbol (Type_Int, _, llvm_id)) -> Type_Int, llvm_id
            | Some (VariableSymbol (_, _, _)) -> failwith ("Variable " ^ id ^ " is not an Int")
            | _ -> failwith ("Undeclared variable: " ^ id)
          in
          empty_ir, LLVM_var (llvm_id), t)