(* TODO : extend when you extend the language *)

type ident = string

type llvm_ident = string

type typ =
  | Type_Void
  | Type_Int
  | Type_Int_Array of int

type variable = Variable of string * expression option

and declaration = 
  | Declarations of declaration list
  | VariableDeclaration of variable
  | FunctionDeclaration of typ * string * variable list * instruction
  | ProtoDeclaration of typ * string * variable list

and expression =
  | IntegerExpression of int
  | StringExpression of string
  | CallExpression of string * expression list * typ
  | AddExpression of expression * expression
  | SubExpression of expression * expression
  | MulExpression of expression * expression
  | DivExpression of expression * expression
  | LoadExpression of variable
  | TernaireExpression of expression * expression * expression

and instruction =
  | BlockInstruction of declaration list * instruction list
  | AssignInstruction of variable * expression
  | WhileInstruction of expression * instruction
  | DoWhileInstruction of instruction * expression
  | ForInstruction of variable * expression * expression * instruction
  | IfInstruction of expression * instruction * instruction option
  | ReturnInstruction of expression
  | CallInstruction of expression
  | PrintInstruction of expression list
  | ReadInstruction of variable list

type program = Program of declaration list
