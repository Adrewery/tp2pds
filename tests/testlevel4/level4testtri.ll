; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [13 x i8] c"\0A Entrer le \00"
@.str2 = global [7 x i8] c"eme:  \00"
@.fmt3 = global [7 x i8] c"%s%d%s\00"
@.fmt4 = global [3 x i8] c"%d\00"
@.str5 = global [5 x i8] c"\0A t[\00"
@.str6 = global [5 x i8] c"] = \00"
@.fmt7 = global [9 x i8] c"%s%d%s%d\00"


define void @main() {
%tmp1 = alloca [10 x i32]
%tmp2 = alloca i32
%tmp3 = alloca i32
store i32 0, i32* %tmp2
br label %while1

while1:
%tmp4 = load i32, i32* %tmp2
%tmp5 = sub i32 10, %tmp4
%tmp14 = icmp ne i32 %tmp5, 0
br i1 %tmp14, label %do2, label %done3

do2:
%tmp6 = getelementptr [13 x i8], [13 x i8]* @.str1, i64 0, i32 0
%tmp7 = load i32, i32* %tmp2
%tmp8 = getelementptr [7 x i8], [7 x i8]* @.str2, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt3, i64 0, i64 0), i8* %tmp6, i32 %tmp7, i8* %tmp8)
call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt4, i64 0, i64 0), i32* %tmp3)
%tmp9 = load i32, i32* %tmp2
%tmp10 = getelementptr [10 x i32], [10 x i32]* %tmp1, i64 0, i32 %tmp9
%tmp11 = load i32, i32* %tmp3
store i32 %tmp11, i32* %tmp10
%tmp12 = load i32, i32* %tmp2
%tmp13 = add i32 %tmp12, 1
store i32 %tmp13, i32* %tmp2
br label %while1

done3:
%tmp15 = getelementptr [10 x i32], [10 x i32]* %tmp1, i64 0, i32 0
call void @naivesort(i32* %tmp15, i32 9)
store i32 0, i32* %tmp2
br label %while4

while4:
%tmp16 = load i32, i32* %tmp2
%tmp17 = sub i32 10, %tmp16
%tmp26 = icmp ne i32 %tmp17, 0
br i1 %tmp26, label %do5, label %done6

do5:
%tmp18 = getelementptr [5 x i8], [5 x i8]* @.str5, i64 0, i32 0
%tmp19 = load i32, i32* %tmp2
%tmp20 = getelementptr [5 x i8], [5 x i8]* @.str6, i64 0, i32 0
%tmp22 = load i32, i32* %tmp2
%tmp23 = getelementptr [10 x i32], [10 x i32]* %tmp1, i64 0, i32 %tmp22
%tmp21 = load i32, i32* %tmp23
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.fmt7, i64 0, i64 0), i8* %tmp18, i32 %tmp19, i8* %tmp20, i32 %tmp21)
%tmp24 = load i32, i32* %tmp2
%tmp25 = add i32 %tmp24, 1
store i32 %tmp25, i32* %tmp2
br label %while4

done6:
ret void
}
define void @naivesort(i32* %tmp28, i32 %tmp30) {
%tmp27 = alloca i32*
store i32* %tmp28, i32** %tmp27
%tmp29 = alloca i32
store i32 %tmp30, i32* %tmp29
%tmp31 = alloca i32
%tmp32 = alloca i32
%tmp33 = alloca i32
%tmp34 = load i32, i32* %tmp29
%tmp35 = icmp ne i32 %tmp34, 0
br i1 %tmp35, label %then7, label %fi8

then7:
%tmp37 = load i32, i32* %tmp29
%tmp39 = load i32*, i32** %tmp27
%tmp38 = getelementptr i32, i32* %tmp39, i32 %tmp37
%tmp36 = load i32, i32* %tmp38
store i32 %tmp36, i32* %tmp31
%tmp40 = load i32, i32* %tmp29
store i32 %tmp40, i32* %tmp33
%tmp41 = load i32, i32* %tmp29
store i32 %tmp41, i32* %tmp32
br label %while11

while11:
%tmp42 = load i32, i32* %tmp33
%tmp43 = add i32 %tmp42, 1
%tmp58 = icmp ne i32 %tmp43, 0
br i1 %tmp58, label %do12, label %done13

do12:
%tmp45 = load i32, i32* %tmp33
%tmp47 = load i32*, i32** %tmp27
%tmp46 = getelementptr i32, i32* %tmp47, i32 %tmp45
%tmp44 = load i32, i32* %tmp46
%tmp48 = load i32, i32* %tmp31
%tmp49 = call i32 @plusgrandstrict(i32 %tmp44, i32 %tmp48)
%tmp50 = icmp ne i32 %tmp49, 0
br i1 %tmp50, label %then9, label %fi10

then9:
%tmp52 = load i32, i32* %tmp33
%tmp54 = load i32*, i32** %tmp27
%tmp53 = getelementptr i32, i32* %tmp54, i32 %tmp52
%tmp51 = load i32, i32* %tmp53
store i32 %tmp51, i32* %tmp31
%tmp55 = load i32, i32* %tmp33
store i32 %tmp55, i32* %tmp32
br label %fi10

fi10:
%tmp56 = load i32, i32* %tmp33
%tmp57 = sub i32 %tmp56, 1
store i32 %tmp57, i32* %tmp33
br label %while11

done13:
%tmp59 = load i32, i32* %tmp32
%tmp61 = load i32*, i32** %tmp27
%tmp60 = getelementptr i32, i32* %tmp61, i32 %tmp59
%tmp63 = load i32, i32* %tmp29
%tmp65 = load i32*, i32** %tmp27
%tmp64 = getelementptr i32, i32* %tmp65, i32 %tmp63
%tmp62 = load i32, i32* %tmp64
store i32 %tmp62, i32* %tmp60
%tmp66 = load i32, i32* %tmp29
%tmp68 = load i32*, i32** %tmp27
%tmp67 = getelementptr i32, i32* %tmp68, i32 %tmp66
%tmp69 = load i32, i32* %tmp31
store i32 %tmp69, i32* %tmp67
%tmp70 = load i32*, i32** %tmp27
%tmp71 = load i32, i32* %tmp29
%tmp72 = sub i32 %tmp71, 1
call void @naivesort(i32* %tmp70, i32 %tmp72)
br label %fi8

fi8:
ret void
}
define i32 @plusgrandstrict(i32 %tmp74, i32 %tmp76) {
%tmp73 = alloca i32
store i32 %tmp74, i32* %tmp73
%tmp75 = alloca i32
store i32 %tmp76, i32* %tmp75
%tmp77 = alloca i32
%tmp78 = alloca i32
%tmp79 = alloca i32
%tmp80 = load i32, i32* %tmp73
%tmp81 = load i32, i32* %tmp75
%tmp82 = mul i32 %tmp80, %tmp81
store i32 %tmp82, i32* %tmp77
%tmp83 = load i32, i32* %tmp73
store i32 %tmp83, i32* %tmp78
%tmp84 = load i32, i32* %tmp75
store i32 %tmp84, i32* %tmp79
br label %while14

while14:
%tmp85 = load i32, i32* %tmp77
%tmp93 = icmp ne i32 %tmp85, 0
br i1 %tmp93, label %do15, label %done16

do15:
%tmp86 = load i32, i32* %tmp79
%tmp87 = sub i32 %tmp86, 1
store i32 %tmp87, i32* %tmp79
%tmp88 = load i32, i32* %tmp78
%tmp89 = sub i32 %tmp88, 1
store i32 %tmp89, i32* %tmp78
%tmp90 = load i32, i32* %tmp78
%tmp91 = load i32, i32* %tmp79
%tmp92 = mul i32 %tmp90, %tmp91
store i32 %tmp92, i32* %tmp77
br label %while14

done16:
%tmp94 = load i32, i32* %tmp78
%tmp95 = icmp ne i32 %tmp94, 0
br i1 %tmp95, label %then17, label %else19

then17:
ret i32 1
br label %fi18

else19:
ret i32 0
br label %fi18

fi18:
ret i32 0
}

