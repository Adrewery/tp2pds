; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str4 = global [19 x i8] c" est different de \00"
@.str5 = global [2 x i8] c"\0A\00"
@.fmt6 = global [9 x i8] c"%d%s%d%s\00"
@.str1 = global [13 x i8] c" est egal a \00"
@.str2 = global [2 x i8] c"\0A\00"
@.fmt3 = global [9 x i8] c"%d%s%d%s\00"


define void @main() {
call void @compare(i32 2, i32 1)
call void @compare(i32 1, i32 2)
call void @compare(i32 1, i32 1)
ret void
}
define void @compare(i32 %tmp2, i32 %tmp4) {
%tmp1 = alloca i32
store i32 %tmp2, i32* %tmp1
%tmp3 = alloca i32
store i32 %tmp4, i32* %tmp3
%tmp5 = load i32, i32* %tmp1
%tmp6 = load i32, i32* %tmp3
%tmp7 = sub i32 %tmp5, %tmp6
%tmp8 = icmp ne i32 %tmp7, 0
br i1 %tmp8, label %then1, label %else3

then1:
%tmp13 = load i32, i32* %tmp1
%tmp14 = getelementptr [19 x i8], [19 x i8]* @.str4, i64 0, i32 0
%tmp15 = load i32, i32* %tmp3
%tmp16 = getelementptr [2 x i8], [2 x i8]* @.str5, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.fmt6, i64 0, i64 0), i32 %tmp13, i8* %tmp14, i32 %tmp15, i8* %tmp16)
br label %fi2

else3:
%tmp9 = load i32, i32* %tmp1
%tmp10 = getelementptr [13 x i8], [13 x i8]* @.str1, i64 0, i32 0
%tmp11 = load i32, i32* %tmp3
%tmp12 = getelementptr [2 x i8], [2 x i8]* @.str2, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.fmt3, i64 0, i64 0), i32 %tmp9, i8* %tmp10, i32 %tmp11, i8* %tmp12)
br label %fi2

fi2:
ret void
}

