; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [7 x i8] c"1+3 = \00"
@.fmt2 = global [5 x i8] c"%s%d\00"


define void @main() {
%tmp1 = getelementptr [7 x i8], [7 x i8]* @.str1, i64 0, i32 0
%tmp2 = call i32 @plus(i32 1, i32 3)
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.fmt2, i64 0, i64 0), i8* %tmp1, i32 %tmp2)
ret void
}
define i32 @plus(i32 %tmp4, i32 %tmp6) {
%tmp3 = alloca i32
store i32 %tmp4, i32* %tmp3
%tmp5 = alloca i32
store i32 %tmp6, i32* %tmp5
%tmp7 = load i32, i32* %tmp3
%tmp8 = load i32, i32* %tmp5
%tmp9 = add i32 %tmp7, %tmp8
ret i32 %tmp9
ret i32 0
}

