; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.fmt1 = global [3 x i8] c"%d\00"
@.str2 = global [18 x i8] c"Le nombre lu est \00"
@.fmt3 = global [5 x i8] c"%s%d\00"


define void @main() {
%tmp1 = alloca i32
call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt1, i64 0, i64 0), i32* %tmp1)
%tmp2 = getelementptr [18 x i8], [18 x i8]* @.str2, i64 0, i32 0
%tmp3 = load i32, i32* %tmp1
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.fmt3, i64 0, i64 0), i8* %tmp2, i32 %tmp3)
ret void
}

