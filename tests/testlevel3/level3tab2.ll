; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.fmt1 = global [7 x i8] c"%d%d%d\00"
@.str2 = global [8 x i8] c"t[0] = \00"
@.str3 = global [9 x i8] c"\0At[1] = \00"
@.str4 = global [9 x i8] c"\0At[2] = \00"
@.fmt5 = global [13 x i8] c"%s%d%s%d%s%d\00"


define void @main() {
%tmp1 = alloca i32
%tmp2 = alloca i32
%tmp3 = alloca i32
%tmp4 = alloca [3 x i32]
call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt1, i64 0, i64 0), i32* %tmp1, i32* %tmp2, i32* %tmp3)
%tmp5 = getelementptr [3 x i32], [3 x i32]* %tmp4, i64 0, i32 0
%tmp6 = load i32, i32* %tmp1
store i32 %tmp6, i32* %tmp5
%tmp7 = getelementptr [3 x i32], [3 x i32]* %tmp4, i64 0, i32 1
%tmp8 = load i32, i32* %tmp2
store i32 %tmp8, i32* %tmp7
%tmp9 = getelementptr [3 x i32], [3 x i32]* %tmp4, i64 0, i32 2
%tmp10 = load i32, i32* %tmp3
store i32 %tmp10, i32* %tmp9
%tmp11 = getelementptr [8 x i8], [8 x i8]* @.str2, i64 0, i32 0
%tmp13 = getelementptr [3 x i32], [3 x i32]* %tmp4, i64 0, i32 0
%tmp12 = load i32, i32* %tmp13
%tmp14 = getelementptr [9 x i8], [9 x i8]* @.str3, i64 0, i32 0
%tmp16 = getelementptr [3 x i32], [3 x i32]* %tmp4, i64 0, i32 1
%tmp15 = load i32, i32* %tmp16
%tmp17 = getelementptr [9 x i8], [9 x i8]* @.str4, i64 0, i32 0
%tmp19 = getelementptr [3 x i32], [3 x i32]* %tmp4, i64 0, i32 2
%tmp18 = load i32, i32* %tmp19
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt5, i64 0, i64 0), i8* %tmp11, i32 %tmp12, i8* %tmp14, i32 %tmp15, i8* %tmp17, i32 %tmp18)
ret void
}

