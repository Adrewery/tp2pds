; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.fmt1 = global [3 x i8] c"%d\00"
@.str2 = global [19 x i8] c"Tableau de taille \00"
@.str3 = global [5 x i8] c" = [\00"
@.fmt4 = global [7 x i8] c"%s%d%s\00"
@.str5 = global [2 x i8] c",\00"
@.fmt6 = global [3 x i8] c"%s\00"
@.fmt7 = global [3 x i8] c"%d\00"
@.str8 = global [3 x i8] c"]\0A\00"
@.fmt9 = global [3 x i8] c"%s\00"


define void @main() {
%tmp1 = alloca i32
%tmp2 = alloca [8 x i32]
%tmp3 = alloca i32
store i32 0, i32* %tmp1
br label %while1

while1:
%tmp4 = load i32, i32* %tmp1
%tmp5 = sub i32 8, %tmp4
%tmp11 = icmp ne i32 %tmp5, 0
br i1 %tmp11, label %do2, label %done3

do2:
call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt1, i64 0, i64 0), i32* %tmp3)
%tmp6 = load i32, i32* %tmp1
%tmp7 = getelementptr [8 x i32], [8 x i32]* %tmp2, i64 0, i32 %tmp6
%tmp8 = load i32, i32* %tmp3
store i32 %tmp8, i32* %tmp7
%tmp9 = load i32, i32* %tmp1
%tmp10 = add i32 %tmp9, 1
store i32 %tmp10, i32* %tmp1
br label %while1

done3:
%tmp12 = getelementptr [8 x i32], [8 x i32]* %tmp2, i64 0, i32 0
call void @printtab(i32 8, i32* %tmp12)
ret void
}
define void @printtab(i32 %tmp14, i32* %tmp16) {
%tmp13 = alloca i32
store i32 %tmp14, i32* %tmp13
%tmp15 = alloca i32*
store i32* %tmp16, i32** %tmp15
%tmp17 = alloca i32
%tmp18 = getelementptr [19 x i8], [19 x i8]* @.str2, i64 0, i32 0
%tmp19 = load i32, i32* %tmp13
%tmp20 = getelementptr [5 x i8], [5 x i8]* @.str3, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt4, i64 0, i64 0), i8* %tmp18, i32 %tmp19, i8* %tmp20)
store i32 0, i32* %tmp17
br label %while6

while6:
%tmp21 = load i32, i32* %tmp13
%tmp22 = load i32, i32* %tmp17
%tmp23 = sub i32 %tmp21, %tmp22
%tmp33 = icmp ne i32 %tmp23, 0
br i1 %tmp33, label %do7, label %done8

do7:
%tmp24 = load i32, i32* %tmp17
%tmp25 = icmp ne i32 %tmp24, 0
br i1 %tmp25, label %then4, label %fi5

then4:
%tmp26 = getelementptr [2 x i8], [2 x i8]* @.str5, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt6, i64 0, i64 0), i8* %tmp26)
br label %fi5

fi5:
%tmp28 = load i32, i32* %tmp17
%tmp30 = load i32*, i32** %tmp15
%tmp29 = getelementptr i32, i32* %tmp30, i32 %tmp28
%tmp27 = load i32, i32* %tmp29
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt7, i64 0, i64 0), i32 %tmp27)
%tmp31 = load i32, i32* %tmp17
%tmp32 = add i32 %tmp31, 1
store i32 %tmp32, i32* %tmp17
br label %while6

done8:
%tmp34 = getelementptr [3 x i8], [3 x i8]* @.str8, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt9, i64 0, i64 0), i8* %tmp34)
ret void
}

