; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [10 x i8] c"%d = %s%s\00"
@.str2 = global [14 x i8] c"quarante-deux\00"
@.str3 = global [2 x i8] c"\0A\00"
@.fmt4 = global [3 x i8] c"%s\00"
@.str5 = global [12 x i8] c"abc %s def\0A\00"
@.str6 = global [6 x i8] c"%d %d\00"
@.fmt7 = global [3 x i8] c"%s\00"


define void @main() {
%tmp1 = getelementptr [10 x i8], [10 x i8]* @.str1, i64 0, i32 0
%tmp2 = getelementptr [14 x i8], [14 x i8]* @.str2, i64 0, i32 0
%tmp3 = getelementptr [2 x i8], [2 x i8]* @.str3, i64 0, i32 0
%tmp6 = call i32 @strlen(i8* %tmp1)
%tmp5 = add i32 1, %tmp6
%tmp7 = add i32 %tmp5, 11
%tmp9 = call i32 @strlen(i8* %tmp2)
%tmp8 = add i32 %tmp7, %tmp9
%tmp11 = call i32 @strlen(i8* %tmp3)
%tmp10 = add i32 %tmp8, %tmp11
%tmp4 = call i8* @malloc(i32 %tmp10)
call void (i8*, i8*, ...) @sprintf(i8* %tmp4, i8* %tmp1, i32 42, i8* %tmp2, i8* %tmp3)
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt4, i64 0, i64 0), i8* %tmp4)
%tmp12 = getelementptr [12 x i8], [12 x i8]* @.str5, i64 0, i32 0
%tmp13 = getelementptr [6 x i8], [6 x i8]* @.str6, i64 0, i32 0
%tmp16 = call i32 @strlen(i8* %tmp12)
%tmp15 = add i32 1, %tmp16
%tmp18 = call i32 @strlen(i8* %tmp13)
%tmp17 = add i32 %tmp15, %tmp18
%tmp14 = call i8* @malloc(i32 %tmp17)
call void (i8*, i8*, ...) @sprintf(i8* %tmp14, i8* %tmp12, i8* %tmp13)
%tmp21 = call i32 @strlen(i8* %tmp14)
%tmp20 = add i32 1, %tmp21
%tmp22 = add i32 %tmp20, 11
%tmp23 = add i32 %tmp22, 11
%tmp19 = call i8* @malloc(i32 %tmp23)
call void (i8*, i8*, ...) @sprintf(i8* %tmp19, i8* %tmp14, i32 17, i32 42)
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt7, i64 0, i64 0), i8* %tmp19)
ret void
}

