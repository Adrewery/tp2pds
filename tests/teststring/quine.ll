; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [6 x i8] c"%s\22%s\00"
@.str2 = global [1 x i8] c"\00"
@.str3 = global [1 x i8] c"\00"
@.str4 = global [172 x i8] c"\0APRINT \22FUNC STRING q() RETURN format(\22,q(),q(),q(),\22%s\22,q(),\22%s\22,q(),q(),q(),\22, \22,q(),q(),\22, \22,q(),q(),\22) FUNC VOID main() { STRING s s := \22,q(),q(),q(),s,q(),q(),q(),s}\0A\00"
@.str5 = global [31 x i8] c"FUNC STRING q() RETURN format(\00"
@.str6 = global [3 x i8] c"%s\00"
@.str7 = global [3 x i8] c"%s\00"
@.str8 = global [3 x i8] c", \00"
@.str9 = global [3 x i8] c", \00"
@.str10 = global [36 x i8] c") FUNC VOID main() { STRING s s := \00"
@.fmt11 = global [51 x i8] c"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\00"


define i8* @q() {
%tmp1 = getelementptr [6 x i8], [6 x i8]* @.str1, i64 0, i32 0
%tmp2 = getelementptr [1 x i8], [1 x i8]* @.str2, i64 0, i32 0
%tmp3 = getelementptr [1 x i8], [1 x i8]* @.str3, i64 0, i32 0
%tmp6 = call i32 @strlen(i8* %tmp1)
%tmp5 = add i32 1, %tmp6
%tmp8 = call i32 @strlen(i8* %tmp2)
%tmp7 = add i32 %tmp5, %tmp8
%tmp10 = call i32 @strlen(i8* %tmp3)
%tmp9 = add i32 %tmp7, %tmp10
%tmp4 = call i8* @malloc(i32 %tmp9)
call void (i8*, i8*, ...) @sprintf(i8* %tmp4, i8* %tmp1, i8* %tmp2, i8* %tmp3)
ret i8* %tmp4
ret i8* null
}
define void @main() {
%tmp11 = alloca i8*
%tmp12 = getelementptr [172 x i8], [172 x i8]* @.str4, i64 0, i32 0
store i8* %tmp12, i8** %tmp11
%tmp13 = getelementptr [31 x i8], [31 x i8]* @.str5, i64 0, i32 0
%tmp14 = call i8* @q()
%tmp15 = call i8* @q()
%tmp16 = call i8* @q()
%tmp17 = getelementptr [3 x i8], [3 x i8]* @.str6, i64 0, i32 0
%tmp18 = call i8* @q()
%tmp19 = getelementptr [3 x i8], [3 x i8]* @.str7, i64 0, i32 0
%tmp20 = call i8* @q()
%tmp21 = call i8* @q()
%tmp22 = call i8* @q()
%tmp23 = getelementptr [3 x i8], [3 x i8]* @.str8, i64 0, i32 0
%tmp24 = call i8* @q()
%tmp25 = call i8* @q()
%tmp26 = getelementptr [3 x i8], [3 x i8]* @.str9, i64 0, i32 0
%tmp27 = call i8* @q()
%tmp28 = call i8* @q()
%tmp29 = getelementptr [36 x i8], [36 x i8]* @.str10, i64 0, i32 0
%tmp30 = call i8* @q()
%tmp31 = call i8* @q()
%tmp32 = call i8* @q()
%tmp33 = load i8*, i8** %tmp11
%tmp34 = call i8* @q()
%tmp35 = call i8* @q()
%tmp36 = call i8* @q()
%tmp37 = load i8*, i8** %tmp11
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.fmt11, i64 0, i64 0), i8* %tmp13, i8* %tmp14, i8* %tmp15, i8* %tmp16, i8* %tmp17, i8* %tmp18, i8* %tmp19, i8* %tmp20, i8* %tmp21, i8* %tmp22, i8* %tmp23, i8* %tmp24, i8* %tmp25, i8* %tmp26, i8* %tmp27, i8* %tmp28, i8* %tmp29, i8* %tmp30, i8* %tmp31, i8* %tmp32, i8* %tmp33, i8* %tmp34, i8* %tmp35, i8* %tmp36, i8* %tmp37)
ret void
}

