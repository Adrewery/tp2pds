; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [5 x i8] c"toto\00"
@.fmt2 = global [3 x i8] c"%s\00"
@.str3 = global [5 x i8] c"titi\00"
@.fmt4 = global [3 x i8] c"%s\00"


define void @main() {
%tmp1 = getelementptr [5 x i8], [5 x i8]* @.str1, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt2, i64 0, i64 0), i8* %tmp1)
%tmp2 = getelementptr [5 x i8], [5 x i8]* @.str3, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt4, i64 0, i64 0), i8* %tmp2)
ret void
}

